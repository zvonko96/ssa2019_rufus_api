﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Attendance.BusinessLayer;
using Attendance.DataLayer.Models;
using Attendance.DataLayer.Repos;

namespace Attendance
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public static string ConnectionString { get; set; }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddScoped<IStudentBusiness, StudentBusiness>();
            services.AddScoped<IStudentRepository, StudentRepository>();

            services.AddScoped<ICourseAttendanceRepo, CourseAttendanceRepo>();
            services.AddScoped<ICourseAttendanceBusiness, CourseAttendanceBusiness>();

            services.AddScoped<IClassRepository, ClassRepository>();
            services.AddScoped<IClassBusiness, ClassBusiness>();

            services.AddScoped<ISubjectBusiness, SubjectBusiness>();
            services.AddScoped<ISubjectRepository, SubjectRepository>();

            services.AddScoped<ILecturerBusiness, LecturerBussiness>();
            services.AddScoped<ILecturerRepository, LecturerRepository>();

            services.AddScoped<ILoginBusiness, LoginBusiness>();

            services.AddScoped<IAttendanceRepository, AttendanceRepository>();
            services.AddScoped<IAttendanceBusiness, AttendanceBusiness>();



            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }


            ConnectionString = Configuration["ConnectionString"];

            app.UseHttpsRedirection();
            app.UseMvc();

        }
    }
}
