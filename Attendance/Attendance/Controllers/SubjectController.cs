﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attendance.BusinessLayer;
using Attendance.DataLayer.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Attendance.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("MyPolicy")]
    [ApiController]
    public class SubjectController : ControllerBase
    {
        private ISubjectBusiness subjectBusiness;

        public SubjectController(ISubjectBusiness subjectBusiness)
        {
            this.subjectBusiness = subjectBusiness;
            this.subjectBusiness.GetConnectionString(Startup.ConnectionString);
        }

        // metoda koja vraća listu svih predmeta
        // GET: api/subject
        [HttpGet]
        public List<Subject> GetAllSubjects()
        {
            return subjectBusiness.GetAllSubjects();
        }

        // GET: api/subject/5
        // metoda koja vraca predmet po ID profesora
        [HttpGet("{id}")]
        public List<Subject> getSubjectNameByLecturerID(int id)
        {
            return subjectBusiness.getSubjectNameByLecturerID(id);
        }

        // GET: api/subject/subjectName/3
        // metoda koja vraca predmet po ID istog
        [HttpGet("subjectName/{id}")]
        public string getSubjectNameById(int id)
        {
            return this.subjectBusiness.getSubjectNameById(id);
        }
    }
}