﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Attendance.BusinessLayer;
using Attendance.DataLayer.Models;
using Attendance.DataLayer.Repos;
using Microsoft.AspNetCore.Cors;

namespace Attendance.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("MyPolicy")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private IStudentBusiness studentBusiness;

        public StudentController(IStudentBusiness studentBusiness)
        {
            this.studentBusiness = studentBusiness;
            this.studentBusiness.GetConnectionString(Startup.ConnectionString);
        }

        // metoda koja vraća listu svih studenata
        // GET: api/student
        [HttpGet]
        public List<Student> GetAllStudents()
        {
            return this.studentBusiness.GetAllStudents();
        }
        // GET: api/student/5
        [HttpGet("{id}")]
        public Student GetById(int id)
        {
            return this.studentBusiness.GetStudentByID(id);
        }
        //Metoda koja hashuje password za postojeceg korisnika
        [Route("{id}/hashpw")]
        [HttpPut]
        public bool HashPass(int id)
        {
            return this.studentBusiness.hashpw(id);
        }

        // POST: api/student/insert
        [Route("insert")]
        [HttpPost]
        public bool InsertStudent([FromBody] Student s)
        {
            return this.studentBusiness.InsertStudent(s);
        }

        // POST: api/student/login
        [Route("login")]
        [HttpPost]
        public Student LoginAttempt([FromBody]LoginAttempt a)
        {
            return this.studentBusiness.LoginAttempt(a);
        }
        // GET: api/student/mail/test@test.com
        [HttpGet("mail/{email}")]
        public Student GetById(string email)
        {
            return this.studentBusiness.GetStudentByEmail(email);
        }

        //DELETE: api/student/delete
        [Route("delete/{studentID}")]
        [HttpDelete]
        public bool DeleteStudent(int studentID)
        {
            return this.studentBusiness.deleteStudent(studentID);
        }

        // PUT: api/student/update
        [Route("update")]
        [HttpPut]
        public bool UpdateStudent([FromBody] Student s)
        {
            return this.studentBusiness.UpdateStudent(s);
        }

    }
}
