﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attendance.BusinessLayer;
using Attendance.DataLayer.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Attendance.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("MyPolicy")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private ILoginBusiness loginBusiness;
        public LoginController(ILoginBusiness loginBusiness)
        {
            this.loginBusiness = loginBusiness;
            this.loginBusiness.GetConnectionString(Startup.ConnectionString);
        }
        // GET: api/login
        [HttpPost]
        public LoginResponse TryLogin([FromBody]LoginAttempt attempt)
        {
            return this.loginBusiness.TryLogin(attempt);
        }
        /*Pogledati Attendance.Model.LoginAttempt
         Ukoliko je attempt.IsLoginSuccessful = false znaci da ne postoji u bazi ni jedan student ni profesor sa tim login parametrima
         Ukoliko je true, gleda se polje attempt.isStudent koje je true ako je student i false ako je korisnik profesor.*/

    }
}
