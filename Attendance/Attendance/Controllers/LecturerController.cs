﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Attendance.BusinessLayer;
using Attendance.DataLayer.Models;
using Attendance.DataLayer.Repos;
using Microsoft.AspNetCore.Cors;

namespace Attendance.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("MyPolicy")]
    [ApiController]
    public class LecturerController : ControllerBase
    {
        private ILecturerBusiness lecturerBusiness;

        public LecturerController(ILecturerBusiness lecturerBusiness)
        {
            this.lecturerBusiness = lecturerBusiness;
            this.lecturerBusiness.GetConnectionString(Startup.ConnectionString);
        }

        // metoda koja vraća listu svih profesora
        // GET: api/lecturer
        [HttpGet]
        public List<Lecturer> GetAllLecturers()
        {
            return this.lecturerBusiness.GetAllLecturers();
        }

        // GET: api/lecturer/5
        // metoda koja vraca profesora po ID
        [HttpGet("{id}")]
        public Lecturer getById(int id)
        {
            return this.lecturerBusiness.getLecturerById(id);
        }

        // POST: api/lecturer/login
        //login profesora prema email i pass
        [Route("login")]
        [HttpPost]
        public Lecturer loginAttempt([FromBody]LoginAttempt a)
        {
            return this.lecturerBusiness.loginAttempt(a);
        }


    }
}
