﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Attendance.BusinessLayer;
using Attendance.DataLayer.Models;
using Attendance.DataLayer.Repos;

namespace Attendance.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("MyPolicy")]
    [ApiController]
    public class ClassController : ControllerBase
    {
        private IClassBusiness classBusiness;
        public ClassController(IClassBusiness cb)
        {
            this.classBusiness = cb;
            this.classBusiness.GetConnectionString(Startup.ConnectionString);
        }
        // GET: api/Class
        [HttpGet]
        public List<Class> GetAllClasses()
        {
            return this.classBusiness.GetAllClasses();
        }
        // GET: api/class/date/{datum}
        [HttpGet("date/{date}")]
        public List<Class> GetByDate(DateTime date)
        {
            return this.classBusiness.GetAllClassesByDay(date);
        }
        // GET: api/class/today/{studentID}
        [HttpGet("today/{studentID}")]
        public List<Class> GetByDate(int studentID)
        {
            return this.classBusiness.GetAllOpenClassesTodayForStudentID(studentID);
        }

        // POST: api/class/insert
        [Route("insert")]
        [HttpPost]
        public bool InsertClass([FromBody] Class c)
        {
            return this.classBusiness.insertClass(c);
        }

        // PUT: api/class/update
        [Route("update")]
        [HttpPut]
        public bool UpdateClass([FromBody] Class c)
        {
            return this.classBusiness.updateClass(c);
        }

        //DELETE: api/class/delete
        [Route("delete/{classID}")]
        [HttpDelete]
        public bool DeleteClass(int classID)
        {
            return this.classBusiness.deleteClass(classID);
        }

        // GET: api/class/today
        [HttpGet("today")]
        public List<Class_Subject> getAllClassesForDay()
        {
            return this.classBusiness.getAllClassesForDay();
        }
    }
}
