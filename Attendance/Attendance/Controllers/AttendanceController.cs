﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Attendance.BusinessLayer;
using Attendance.DataLayer.Models;
using Attendance.DataLayer.Repos;

namespace Attendance.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("MyPolicy")]
    [ApiController]
    public class AttendanceController : ControllerBase
    {
        private IAttendanceBusiness attBusiness;

        public AttendanceController(IAttendanceBusiness at)
        {
            this.attBusiness = at;
            this.attBusiness.GetConnectionString(Startup.ConnectionString);
        }

        // GET: api/Attendance
        [HttpGet]
        public List<Student_Subject_Attendance> getAllAttendance()
        {
            return this.attBusiness.getAllAttendances();
        }

        // POST: api/attendance/insert
        [Route("insert")]
        [HttpPost]
        public bool insertAttendance([FromBody] Attendance1 a)
        {
            return this.attBusiness.insertAttendance(a);
        }

        //DELETE: api/attendance/delete
        [Route("delete/{attendancesID}")]
        [HttpDelete]
        public bool DeleteStudentAte(int attendancesID)
        {
            return this.attBusiness.deleteStudentAte(attendancesID);
        }
    }
}