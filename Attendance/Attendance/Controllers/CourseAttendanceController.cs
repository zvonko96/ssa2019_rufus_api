﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Attendance.BusinessLayer;
using Attendance.DataLayer.Models;
using Attendance.DataLayer.Repos;

namespace Attendance.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("MyPolicy")]
    [ApiController]
    public class CourseAttendanceController : ControllerBase
    {
        private ICourseAttendanceBusiness CABusiness;

        public CourseAttendanceController(ICourseAttendanceBusiness CABusiness)
        {
            this.CABusiness = CABusiness;
            this.CABusiness.GetConnectionString(Startup.ConnectionString);
        }
        // GET: api/CourseAttendance
        [HttpGet]
        public List<CourseAttendance> GetAllCourseAttendances()
        {
            return this.CABusiness.GetAllCourseAttendances();
        }
        // GET: api/courseAttendance/1/student
        [HttpGet("{id}/student")]
        public List<CourseAttendance> GetByStudentId(int id)
        {
            return this.CABusiness.GetAttendancesByStudentID(id);
        }
        // GET: api/courseAttendance/1/subject
        [HttpGet("{id}/subject")]
        public List<CourseAttendance> GetBySubjectId(int id)
        {
            return this.CABusiness.StudentsWhoAttendsSubject(id);
        }

        //INSERT: api/courseAttendance/insert
        [Route("insert")]
        [HttpPost]
        public bool insertCourseAttendance([FromBody] CourseAttendance ca)
        {
            return this.CABusiness.insertCourseAttendance(ca);
        }

        //DELETE: api/courseAttendance/delete/7
        [Route("delete/{courseattendanceID}")]
        [HttpDelete]
        public bool deleteCourseAttendance(int courseattendanceID)
        {
            return this.CABusiness.deleteCourseAttendance(courseattendanceID);
        }
    }
}
