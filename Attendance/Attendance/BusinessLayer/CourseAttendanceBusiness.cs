﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attendance.DataLayer.Repos;
using Attendance.DataLayer.Models;

namespace Attendance.BusinessLayer
{
    public class CourseAttendanceBusiness: ICourseAttendanceBusiness
    {
        private ICourseAttendanceRepo CARepo;
        public void GetConnectionString(string connString)
        {
            this.CARepo.GetConnectionString(connString);
        }
        public CourseAttendanceBusiness(ICourseAttendanceRepo CARepo)
        {
            this.CARepo = CARepo;
        }
        public List<CourseAttendance> GetAllCourseAttendances()
        {
            List<CourseAttendance> CA = CARepo.GetAllAttendances();
            if (CA.Count > 1)
                return CA;
            else
                return null;
        }
        public List<CourseAttendance> GetAttendancesByStudentID(int studentID)
        {
            List<CourseAttendance> CAALL = CARepo.GetAllAttendances();
            //List<CourseAttendance> listToReturn=null;

            if (CAALL.Count > 0)
            {
                return CAALL.FindAll(c => c.studentID == studentID);
                //return listToReturn;
            }
            else
            {
                return null;
            }
        }
        public List<CourseAttendance> StudentsWhoAttendsSubject(int subjectID)
        {
            List<CourseAttendance> CAALL = CARepo.GetAllAttendances();

            if (CAALL.Count > 0)
            {
                return CAALL.FindAll(c => c.subjectID == subjectID);
            }
            else
            {
                return null;
            }
        }

        public bool insertCourseAttendance(CourseAttendance ca)
        {

            List<CourseAttendance> courseattendance = CARepo.GetAllAttendances();


            if (this.CARepo.insertCourseAttendance(ca) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool deleteCourseAttendance(int courseattendanceID)
        {
            bool result = false;
            if (courseattendanceID != 0 && this.CARepo.deleteCourseAttendance(courseattendanceID) > 0)
            {
                result = true;
            }

            return result;
        }
    }
}
