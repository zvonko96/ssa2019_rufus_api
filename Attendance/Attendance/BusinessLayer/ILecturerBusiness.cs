﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attendance.DataLayer.Models;
using Attendance.DataLayer.Repos;

namespace Attendance.BusinessLayer
{
    public interface ILecturerBusiness
    {
        void GetConnectionString(string connString);
        List<Lecturer> GetAllLecturers();
        Lecturer getLecturerById(int id);
        Lecturer loginAttempt(LoginAttempt a);
    }
}
