﻿using Attendance.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Attendance.BusinessLayer
{
    public interface IAttendanceBusiness
    {
         List<Student_Subject_Attendance> getAllAttendances();
         bool insertAttendance(Attendance1 c);
         void GetConnectionString(string connString);
         bool deleteStudentAte(int attendancesID);
    }
}
