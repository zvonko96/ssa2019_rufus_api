﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attendance.DataLayer.Models;

namespace Attendance.BusinessLayer
{
    public interface IClassBusiness
    {
        void GetConnectionString(string connString);
        List<Class> GetAllClasses();
        List<Class> GetAllClassesByDay(DateTime date);
        List<Class> GetAllOpenClassesTodayForStudentID(int studentID);
        bool insertClass(Class c);
        bool updateClass(Class c);
        bool deleteClass(int classID);
        List<Class_Subject> getAllClassesForDay();

    }
}
