﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attendance.DataLayer.Models;
using Attendance.DataLayer.Repos;


namespace Attendance.BusinessLayer
{
    public class LecturerBussiness : ILecturerBusiness
    {

        private ILecturerRepository lecturerRepo;
        public void GetConnectionString(string connString)
        {
            this.lecturerRepo.GetConnectionString(connString);
        }

        public LecturerBussiness(ILecturerRepository lecturerRepo)
        {
            this.lecturerRepo = lecturerRepo;
        }

        public List<Lecturer> GetAllLecturers()
        {
            List<Lecturer> lecturers = lecturerRepo.GetAllLecturers();
            if (lecturers.Count > 1)
                return lecturers;
            else
                return null;

        }

        public Lecturer getLecturerById(int id)
        {
            List<Lecturer> lecturers = lecturerRepo.GetAllLecturers();
            if (lecturers.Count > 0)
            {
                return lecturers.Find(l => l.lecturerID == id); //lambda izrazi
            }
            else
            {
                return null;
            }
        }

        public Lecturer loginAttempt(LoginAttempt a)
        {
            List<Lecturer> lecturers = lecturerRepo.GetAllLecturers();
            if (lecturers.Count > 0)
            {
                return lecturers.Find( l => l.email == a.email && a.pass == l.password); //lambda izrazi
            }
            else
            {
                return null;
            }
        }
    }
}
