﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attendance.DataLayer.Models;
using Attendance.DataLayer.Repos;

namespace Attendance.BusinessLayer
{
    public interface ILoginBusiness
    {
        void GetConnectionString(string connString);
        LoginResponse TryLogin(LoginAttempt attempt);
    }
}
