﻿using Attendance.DataLayer.Models;
using Attendance.DataLayer.Repos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attendance.BusinessLayer;


namespace Attendance.BusinessLayer
{
    public class AttendanceBusiness : IAttendanceBusiness
    {
        private IAttendanceRepository attRepo;

        public void GetConnectionString(string connString)
        {
            this.attRepo.GetConnectionString(connString);
        }

        public AttendanceBusiness (IAttendanceRepository attRepo)
        {
            this.attRepo = attRepo;
        }
        
        public List<Student_Subject_Attendance> getAllAttendances()
        {
            List<Student_Subject_Attendance> attendances = attRepo.getAllAttendances();
            if (attendances.Count > 0)
                return attendances;
            else
                return null;
        }

     

        public bool insertAttendance(Attendance1 c)
        {

            List<Attendance1> attendance = attRepo.getAllAttendances1();


            if (this.attRepo.insertAttendance(c) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool deleteStudentAte(int attendancesID)
        {
            bool result = false;
            if (attendancesID != 0 && this.attRepo.deleteStudentAte(attendancesID) > 0)
            {
                result = true;
            }

            return result;
        }
    }
}
