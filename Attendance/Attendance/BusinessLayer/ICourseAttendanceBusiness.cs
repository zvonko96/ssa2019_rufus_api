﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attendance.DataLayer.Repos;
using Attendance.DataLayer.Models;

namespace Attendance.BusinessLayer
{
    public interface ICourseAttendanceBusiness
    {
        void GetConnectionString(string connString);
        List<CourseAttendance> GetAllCourseAttendances();
        List<CourseAttendance> GetAttendancesByStudentID(int studentID);
        List<CourseAttendance> StudentsWhoAttendsSubject(int subjectID);
        bool insertCourseAttendance(CourseAttendance ca);
        bool deleteCourseAttendance(int courseattendanceID);
    }
}
