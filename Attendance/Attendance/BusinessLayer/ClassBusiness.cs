﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attendance.DataLayer.Models;
using Attendance.DataLayer.Repos;
using Attendance.BusinessLayer;

namespace Attendance.BusinessLayer
{
    public class ClassBusiness : IClassBusiness
    {
        private IClassRepository classRepo;
        private ICourseAttendanceBusiness courseAttendanceBusiness;
        

        public void GetConnectionString(string connString)
        {
            this.classRepo.GetConnectionString(connString);
            this.courseAttendanceBusiness.GetConnectionString(connString);
        }
        public ClassBusiness(IClassRepository classRepo, ICourseAttendanceBusiness courseAttendanceBusiness)
        {
            this.classRepo = classRepo;
            this.courseAttendanceBusiness = courseAttendanceBusiness;
        }
        public List<Class> GetAllClasses()
        {
            List<Class> classes = classRepo.GetAllClasses();
            if (classes.Count > 0)
                return classes;
            else
                return null;
        }
        public List<Class> GetAllClassesByDay(DateTime date)
        {
            List<Class> classes = classRepo.GetAllClasses();

            if (classes.Count > 0)
            {
                return classes.FindAll(c => c.date.Date == date.Date);
            }
            else
            {
                return null;
            }
        }
        public List<Class> GetAllOpenClassesTodayForStudentID(int studentID)
        {
            List<Class> classes = classRepo.GetAllClasses();
            List<CourseAttendance> coursesByStudent = courseAttendanceBusiness.GetAttendancesByStudentID(studentID);
            if (classes.Count > 0 || coursesByStudent.Count > 0)
            {
                return classes.FindAll(c => c.date.Date == DateTime.Today && c.isOpen == true && coursesByStudent.Find(CBS => CBS.subjectID == c.subjectID)!= null);
            }
            else
            {
                return null;
            }
        }

        // Unos casova
        public bool insertClass(Class c) 
        {
            
            List<Class> classes = classRepo.GetAllClasses();

            
                if (this.classRepo.insertClass(c) > 0)
                {
                    return true; 
                }
                else
                {
                    return false;
                }
            }

        // Update casova 
        public bool updateClass(Class c)
        {
            bool result = false; // na početku postaviti rezultat na true
            if (c.classID != 0 && this.classRepo.updateClass(c) > 0)
            {
                // ako je prosleđen id casa i izmenjen jedan cas
                result = true;
            }
            return result;
        }

        public bool deleteClass( int classID)
        {
            bool result = false;
            if(classID != 0 && this.classRepo.deleteClass(classID) > 0)
            {
                result = true;
            }

            return result;
        }

        public List<Class_Subject> getAllClassesForDay()
        {
            List<Class_Subject> classes_subjects = classRepo.getAllClassesForDay();
            if (classes_subjects.Count > 0)
                return classes_subjects;
            else
                return null;
        }

    }
}
