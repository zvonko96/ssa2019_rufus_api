﻿using Attendance.DataLayer.Repos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attendance.DataLayer.Models;
using Attendance.BusinessLayer;

namespace Attendance.BusinessLayer
{
    public class SubjectBusiness : ISubjectBusiness
    {
        private ISubjectRepository subjectRepo;
       // private ILecturerBusiness lecturerRepo;

        public void GetConnectionString(string connString)
        {
            this.subjectRepo.GetConnectionString(connString);
        }

        public SubjectBusiness(ISubjectRepository subjectRepo)//, ILecturerBusiness lecturerRepo)
        {
            this.subjectRepo = subjectRepo;
          //  this.lecturerRepo = lecturerRepo;
        }

        public List<Subject> GetAllSubjects()
        {
            List<Subject> subjects = subjectRepo.GetAllSubjects();

            if (subjects.Count > 1)

                return subjects;
            else
                return null;
        }

        public List<Subject> getSubjectNameByLecturerID(int id)
        {
            List<Subject> subjects = subjectRepo.GetAllSubjects();

            if (subjects.Count > 0)
            {

                return subjects.FindAll(s => s.professorID == id);
            }
            else
                return null;

        } 

        public string getSubjectNameById(int id)
        {
             Subject su; //kreiranje objekta klase/modela SUBJECTS

             List<Subject> subjects = subjectRepo.GetAllSubjects();

            if( subjects.Count > 0)
            {
                su = subjects.Find(s => s.subjectID == id);

                return su.subjectName;
            }
            else
            {
                return null;
            }
             

        }

    }
}
