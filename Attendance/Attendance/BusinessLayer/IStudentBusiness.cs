﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attendance.DataLayer.Models;

namespace Attendance.BusinessLayer
{
    public interface IStudentBusiness
    {
        void GetConnectionString(string connString);
        List<Student> GetAllStudents();
        Student GetStudentByID(int id);
        bool UpdateStudent(Student s);
        bool hashpw(int id);
        bool InsertStudent(Student s);
        Student LoginAttempt(LoginAttempt a);
        Student GetStudentByEmail(string mail);
        bool deleteStudent(int studentID);
    }
}
