﻿using Attendance.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Attendance.BusinessLayer
{
    public interface ISubjectBusiness
    {
        void GetConnectionString(string connString);
        List<Subject> GetAllSubjects();
        List<Subject> getSubjectNameByLecturerID(int id);
        string getSubjectNameById(int id);

    }
}
