﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attendance.DataLayer.Models;
using Attendance.DataLayer.Repos;

namespace Attendance.BusinessLayer
{
    public class LoginBusiness:ILoginBusiness
    {
        private ILecturerBusiness lecturerBusiness;
        private IStudentBusiness studentBusiness;
        public void GetConnectionString(string connString)
        {
            this.lecturerBusiness.GetConnectionString(connString);
            this.studentBusiness.GetConnectionString(connString);
        }
        public LoginBusiness(ILecturerBusiness LecBusiness, IStudentBusiness StudBusiness)
        {
            this.lecturerBusiness = LecBusiness;
            this.studentBusiness = StudBusiness;
        }
        public LoginResponse TryLogin(LoginAttempt attempt)
        {
            Student s = studentBusiness.LoginAttempt(attempt);
            Lecturer l = lecturerBusiness.loginAttempt(attempt);
            LoginResponse r = new LoginResponse();
            if (s!=null)
            {
                r.userID = s.studentID;
                r.name = s.name;
                r.surname = s.surname;
                r.studentEmail = s.email;
                r.isStudent = true;
                r.IsLoginSuccessful = true;
            }
            else if (l != null)
            {
                r.userID = l.lecturerID;
                r.name = l.name;
                r.surname = l.surname;
                r.studentEmail = l.email;
                r.isStudent = false;
                r.IsLoginSuccessful = true;
            }
            else
            {
                r.IsLoginSuccessful = false;
            }
            return r;
        }

    }
}
