﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attendance.DataLayer.Models;
using Attendance.DataLayer.Repos;
using Attendance.BusinessLayer;
using Microsoft.AspNetCore.Identity;

namespace Attendance.BusinessLayer
{
    public class StudentBusiness:IStudentBusiness
    {
        private IStudentRepository studentRepo;
        public void GetConnectionString(string connString)
        {
            this.studentRepo.GetConnectionString(connString);
        }

        public StudentBusiness(IStudentRepository studentRepo)
        {
            this.studentRepo = studentRepo;
        }
        public List<Student> GetAllStudents()
        {
            List<Student> students = studentRepo.GetAllStudents();
            if (students.Count > 0)
                return students;
            else
                return null;

        }
        public Student GetStudentByID(int id)
        {
            List<Student> students = studentRepo.GetAllStudents();

            if (students.Count > 0)
            {
                return students.Find(s => s.studentID == id);
            }
            else
            {
                return null;
            }

        }
        public Student GetStudentByEmail(string mail)
        {
            List<Student> students = studentRepo.GetAllStudents();

            if (students.Count > 0)
            {
                return students.Find(s => s.email == mail);
            }
            else
            {
                return null;
            }

        }
        public bool UpdateStudent(Student s)
        {
            bool result = false; // na početku postaviti rezultat na true
            if (s.studentID != 0 && this.studentRepo.UpdateStudent(s) > 0)
            {
                // ako je prosleđen id studenta i izmenjen jedan student
                result = true;
            }
            return result;
        }
        public bool hashpw(int id) //Hashuje password za zadati id studenta u bazi
        {
            Student s = GetStudentByID(id);
            PasswordHasher<string> pw = new PasswordHasher<string>();
            s.pass = pw.HashPassword(s.name, s.pass);
            return UpdateStudent(s);

        }
        public bool InsertStudent(Student s)
        {
            //PasswordHasher<string> pw = new PasswordHasher<string>();
            //s.pass = pw.HashPassword(s.name, s.pass);
            List<Student> students = studentRepo.GetAllStudents();
            if (students.Find(student => student.email == s.email)==null)
            { 
                if (this.studentRepo.InsertStudent(s) > 0)
                {
                    return true; ;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public Student LoginAttempt(LoginAttempt a)
        {
            List<Student> students = studentRepo.GetAllStudents();
            //PasswordHasher<string> ph = new PasswordHasher<string>();
            //Student sP;
            if (students.Count > 0)
            {
                /*sP = students.Find(s => s.email == a.email);
                if(ph.VerifyHashedPassword(sP.name, a.pass, sP.pass) == PasswordVerificationResult.Success)
                {
                    return sP;
                }
                else
                {
                    return null;
                }*/
                return students.Find(s => s.email == a.email && s.pass == a.pass);
            }
            else
            {
                return null;
            }
        }

        public bool deleteStudent(int studentID)
        {
            bool result = false;
            if (studentID != 0 && this.studentRepo.deleteStudent(studentID) > 0)
            {
                result = true;
            }

            return result;
        }

    }
}
