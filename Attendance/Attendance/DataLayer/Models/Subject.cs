﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Attendance.DataLayer.Models
{
    public class Subject
    {
        public int subjectID { get; set; }
        public string subjectName { get; set; }
        public int professorID { get; set; }
        public int assistantID { get; set; }
        public int lectureNo { get; set; }
        public int exerciseNo { get; set; }
        public string subjectDescription { get; set; }
    }
}
