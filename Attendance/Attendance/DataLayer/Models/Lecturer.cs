﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Attendance.DataLayer.Models
{
    public class Lecturer
    {
        public int lecturerID { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string email { get; set; }
        public string cabinetNo { get; set; }
        public string phone { get; set; }
        public string title { get; set; }
        public string password { get; set; }
    }
}
