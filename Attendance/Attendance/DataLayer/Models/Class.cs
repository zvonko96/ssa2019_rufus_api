﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Attendance.DataLayer.Models
{
    public class Class
    {
        public int classID { get; set; }
        public int studentID { get; set; }
        public int subjectID { get; set; }
        public DateTime date { get; set; }
        public bool isLecture { get; set; }
        public int lecturerID { get; set; }
        public bool isOpen { get; set; }
        public string classPass { get; set; }
        public string practiceGroup { get; set; }
    }
}
