﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Attendance.DataLayer.Models
{
    public class Student_Subject_Attendance
    {
        public int attendancesID { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public DateTime time { get; set; }
        public string subjectName { get; set; }
        public int yearNo { get; set; }
        public int indexNo { get; set; }
        public int studentID { get; set; }
        public int classID { get; set; }
    }
}
