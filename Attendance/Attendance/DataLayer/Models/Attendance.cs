﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Attendance.DataLayer.Models
{
    public class Attendance
    {
        int attendanceID { get; set; }
        string time { get; set; }
        int studentID { get; set; }
        int classID { get; set; }
    }
}
