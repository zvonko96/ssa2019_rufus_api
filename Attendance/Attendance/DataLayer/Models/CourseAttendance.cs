﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Attendance.DataLayer.Models
{
    public class CourseAttendance
    {
        public int courseAttendanceID { get; set; }
        public int studentID { get; set; }
        public int subjectID { get; set; }
    }
}
