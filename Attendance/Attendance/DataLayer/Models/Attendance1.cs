﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Attendance.DataLayer.Models
{
    public  class Attendance1
    {
        public int attendanceID { get; set; }
        public DateTime time { get; set; }
        public int studentID { get; set; }
        public int classID { get; set; }
        
    }
}
