﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Attendance.DataLayer.Models
{
    public class Class_Subject
    {
        public string subjectName { get; set; }
        public DateTime date { get; set; }
        public string practiceGroup { get; set; }
        public bool isOpen { get; set; }
        public string classPassword { get; set; }
        public int classID { get; set; }
    }
}
