﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Attendance.DataLayer.Models
{
    public class Department
    {
        int departmentID { get; set; }
        string departmentName { get; set; }
    }
}
