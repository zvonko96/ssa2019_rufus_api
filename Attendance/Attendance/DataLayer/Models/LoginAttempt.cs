﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Attendance.DataLayer.Models
{
    public class LoginAttempt
    {
        public string email { get; set; }
        public string pass { get; set; }
        public bool isStudent { get; set; }
        public bool IsLoginSuccessful { get; set; }
        public int studentID { get; set; }
    }
}
