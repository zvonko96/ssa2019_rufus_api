﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Attendance.DataLayer.Models
{
    public class Student
    {
        public int studentID { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public DateTime dob { get; set; }
        public int yearNo { get; set; }
        public int indexNo { get; set; }
        public string pass { get; set; }
        public int departmentID { get; set; }

        /*public Student(int ID,string name,string surname,string email,string phone,DateTime dob,int yearNo,int indexNo,string pass,int deptNo)
        {
            this.studentID = ID;
            this.name = name;
            this.surname = surname;
            this.email = email;
            this.phone = phone;
            this.dob = dob;
            this.yearNo = yearNo;
            this.indexNo = indexNo;
            this.pass = pass;
            this.departmentID = deptNo;
        }*/
    }
}
