﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Attendance.DataLayer.Models
{
    public class LoginResponse
    {
        public int userID { get; set; }
        public string studentEmail { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public bool isStudent { get; set; }
        public bool IsLoginSuccessful { get; set; }
    }
}
