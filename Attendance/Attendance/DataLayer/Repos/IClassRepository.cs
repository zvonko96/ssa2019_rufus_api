﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attendance.DataLayer.Models;

namespace Attendance.DataLayer.Repos
{
    public interface IClassRepository
    {
        void GetConnectionString(string connString);
        List<Class> GetAllClasses();
        int insertClass(Class c);
        int updateClass(Class c);
        int deleteClass(int classID);
        List<Class_Subject> getAllClassesForDay();
    }
}
