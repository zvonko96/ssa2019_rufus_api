﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Attendance.DataLayer.Models;

namespace Attendance.DataLayer.Repos
{
    public class CourseAttendanceRepo: ICourseAttendanceRepo
    {
        public string ConnectionString;

        public void GetConnectionString(string connString)
        {
            ConnectionString = connString;
        }
        public List<CourseAttendance> GetAllAttendances()
        {
            List<CourseAttendance> listOfAttendances = new List<CourseAttendance>();
            using (SqlConnection dataConnection = new SqlConnection(this.ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = "SELECT * FROM COURSEATTENDANCE";

                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    CourseAttendance c = new CourseAttendance();
                    c.courseAttendanceID = dataReader.GetInt32(0);
                    c.studentID = dataReader.GetInt32(1);
                    c.subjectID = dataReader.GetInt32(2);
                    listOfAttendances.Add(c);
                }
            }

            return listOfAttendances;
        }

        public int insertCourseAttendance(CourseAttendance ca)
        {
            using (SqlConnection dataConnection = new SqlConnection(ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;

                command.CommandText = "INSERT INTO COURSEATTENDANCE VALUES (" + ca.studentID + "," + ca.subjectID + ")";
                System.Diagnostics.Debug.WriteLine("SQL UPIT -----------------" + command.CommandText);
                return command.ExecuteNonQuery();
            }
        }

        public int deleteCourseAttendance(int courseattendanceID)
        {
            using (SqlConnection dataConnection = new SqlConnection(ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;

                command.CommandText = "DELETE FROM COURSEATTENDANCE WHERE courseattendanceID = " + courseattendanceID;
                return command.ExecuteNonQuery();

            }
        }
    }
}
