﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attendance.DataLayer.Models;

namespace Attendance.DataLayer.Repos
{
    public interface ILecturerRepository
    {
        void GetConnectionString(string connString);
        List<Lecturer> GetAllLecturers();
    }
}
