﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Attendance.DataLayer.Models;

namespace Attendance.DataLayer.Repos
{
    public class StudentRepository:IStudentRepository
    {
        public string ConnectionString;

        public void GetConnectionString(string connString)
        {
            ConnectionString = connString;
        }
        
        public List<Student> GetAllStudents()
        {
            List < Student > listOfStudents = new List<Student>();
            using (SqlConnection dataConnection = new SqlConnection(this.ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = "select * from STUDENTS s, CLASSES c, ATTENDANCES a where s.studentID = a.studentID and c.classID = a.classID ";

                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    Student s = new Student();
                    s.studentID = dataReader.GetInt32(0);
                    s.name = dataReader.GetString(1);
                    s.surname = dataReader.GetString(2);
                    s.email = dataReader.GetString(3);
                    s.phone = dataReader.GetString(4);
                    s.dob = dataReader.GetDateTime(5);
                    s.indexNo = dataReader.GetInt32(6);
                    s.yearNo = dataReader.GetInt32(7);
                    s.pass = dataReader.GetString(8).ToString();
                    s.departmentID = dataReader.GetInt32(9);
                    listOfStudents.Add(s);
                }
            }

            return listOfStudents;
        }
        public int UpdateStudent(Student s)
        {
            using (SqlConnection dataConnection = new SqlConnection(ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = "UPDATE STUDENTS SET" +
                    " name = " + "'" + s.name + "'" + "," +
                    " surname = " + "'" + s.surname + "'" + "," +
                    " email = " + "'" + s.email + "'" + "," + " phone = " + "'" + s.phone + "'" + "," +
                    " dob = " + "'" + s.dob + "'" + "," +  " indexNo = "  + s.indexNo + "," +
                    " yearNo = " +  s.yearNo + "," + " password = " + "'" + s.pass + "'" + "," +
                    " departmentID = " + s.departmentID+ 
                    " WHERE studentID = " + s.studentID;
                System.Diagnostics.Debug.WriteLine(command.CommandText);
                return command.ExecuteNonQuery();
            }
        }

        // Metoda koja vrši unos studenta u bazu podataka
        public int InsertStudent(Student s)
        {
            using (SqlConnection dataConnection = new SqlConnection(ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = "INSERT INTO STUDENTS VALUES (" +
                    "'" + s.name + "'" + ", " +
                    "'" + s.surname + "'" + ", " +
                    "'" + s.email + "'" + ", " +
                    "'" + s.phone + "'" + ", " +
                    "'" + s.dob + "'" + ", " +
                    s.indexNo + ", " +
                    s.yearNo + ", " +
                    "'" + s.pass + "'" + ", " +
                    s.departmentID + ")";
                return command.ExecuteNonQuery();
            }
        }

        public int deleteStudent(int studentID)
        {
            using (SqlConnection dataConnection = new SqlConnection(ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;

                command.CommandText = "DELETE FROM STUDENTS WHERE studentID = " + studentID;
                return command.ExecuteNonQuery();

            }
        }


    }
}

