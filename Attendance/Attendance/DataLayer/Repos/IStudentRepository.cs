﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attendance.DataLayer.Models;

namespace Attendance.DataLayer.Repos
{
    public interface IStudentRepository
    {
        void GetConnectionString(string connString);
        List<Student> GetAllStudents();
        int UpdateStudent(Student s);
        int InsertStudent(Student s);
        int deleteStudent(int studentID);
        //Student GetStudentByID(int ID);

    }
}
