﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Attendance.DataLayer.Models;

namespace Attendance.DataLayer.Repos
{
    public class AttendanceRepository : IAttendanceRepository
    {
        public string ConnectionString;

        public void GetConnectionString(string connString)
        {
            ConnectionString = connString;
        }

        public int insertAttendance(Attendance1 a)
        {
            using (SqlConnection dataConnection = new SqlConnection(ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;

                command.CommandText = "INSERT INTO ATTENDANCES VALUES (" + "'" + a.time + "'" + "," + a.studentID + "," + a.classID + ")";
                System.Diagnostics.Debug.WriteLine("SQL UPIT -----------------" + command.CommandText);
                return command.ExecuteNonQuery();
            }
        }

        public List<Student_Subject_Attendance> getAllAttendances()
        {
            List<Student_Subject_Attendance> listOfAttendances = new List<Student_Subject_Attendance>();
            using (SqlConnection dataConnection = new SqlConnection(this.ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = "select s.name, s.surname, su.subjectName, a.time, s.indexNo, s.yearNo, a.attendancesID, a.studentID, a.classID from STUDENTS s, SUBJECTS su, ATTENDANCES a ,  CLASSES c where s.studentID = a.studentID and c.classID = a.classID and c.subjectID = su.subjectID ";

                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    Student_Subject_Attendance ssa = new Student_Subject_Attendance();

                    ssa.name = dataReader.GetString(0);
                    ssa.surname = dataReader.GetString(1);
                    ssa.subjectName = dataReader.GetString(2);
                    ssa.time = dataReader.GetDateTime(3);
                    ssa.indexNo = dataReader.GetInt32(4);
                    ssa.yearNo = dataReader.GetInt32(5);
                    ssa.attendancesID = dataReader.GetInt32(6);
                    ssa.studentID = dataReader.GetInt32(7);
                    ssa.classID = dataReader.GetInt32(8);

                    listOfAttendances.Add(ssa);
                }
            }

            return listOfAttendances;
        }

        public List<Attendance1> getAllAttendances1()
        {
            List<Attendance1> listOfAttendances = new List<Attendance1>();
            using (SqlConnection dataConnection = new SqlConnection(this.ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = "select a.attendancesID, a.time, s.studentID, c.ClassID from STUDENTS s, CLASSES c, ATTENDANCES a where s.studentID = a.studentID and c.classID = a.classID "; ;

                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    Attendance1 a = new Attendance1();
                    a.attendanceID = dataReader.GetInt32(0);
                    a.time = dataReader.GetDateTime(1);
                    a.studentID = dataReader.GetInt32(2);
                    a.classID = dataReader.GetInt32(3);


                    listOfAttendances.Add(a);
                }
            }

            return listOfAttendances;
        }

        public int deleteStudentAte(int attendancesID)
        {
            using (SqlConnection dataConnection = new SqlConnection(ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;

                command.CommandText = " DELETE FROM ATTENDANCES WHERE attendancesID = " + attendancesID;
                return command.ExecuteNonQuery();

            }
        }

    }
}
