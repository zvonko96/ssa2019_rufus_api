﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attendance.DataLayer.Models;

namespace Attendance.DataLayer.Repos
{
    public interface ICourseAttendanceRepo
    {
        List<CourseAttendance> GetAllAttendances();
        void GetConnectionString(string connString);
        int insertCourseAttendance(CourseAttendance ca);
        int deleteCourseAttendance(int courseattendanceID);
    }
}
