﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Attendance.DataLayer.Models;

namespace Attendance.DataLayer.Repos
{
    public interface IAttendanceRepository
    {
        int insertAttendance(Attendance1 a);
        List<Student_Subject_Attendance> getAllAttendances();
        void GetConnectionString(string connString);
        List<Attendance1> getAllAttendances1();
        int deleteStudentAte(int attendancesID);

    }
}
