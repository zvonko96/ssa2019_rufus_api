﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Attendance.DataLayer.Models;

namespace Attendance.DataLayer.Repos
{
    public class LecturerRepository : ILecturerRepository
    {
        public string ConnectionString;

        public void GetConnectionString(string connString)
        {
            ConnectionString = connString;
        }

        public List<Lecturer> GetAllLecturers()
        {
            List<Lecturer> listOfLecturers = new List<Lecturer>();
            using (SqlConnection dataConnection = new SqlConnection(this.ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = "SELECT * FROM LECTURERS";

                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    Lecturer l = new Lecturer();
                    l.lecturerID = dataReader.GetInt32(0);
                    l.name = dataReader.GetString(1);
                    l.surname = dataReader.GetString(2);
                    l.email = dataReader.GetString(3);
                    l.cabinetNo = dataReader.GetString(4);
                    l.phone = dataReader.GetString(5);
                    l.title = dataReader.GetString(6);
                    l.password = dataReader.GetString(7);
                    listOfLecturers.Add(l);



                }
            }

            return listOfLecturers ;
        }
    }
    }

