﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Attendance.DataLayer.Models;

namespace Attendance.DataLayer.Repos
{
    public class ClassRepository : IClassRepository
    {
        public string ConnectionString;

        public void GetConnectionString(string connString)
        {
            ConnectionString = connString;
        }
        public List<Class> GetAllClasses()
        {
            List<Class> listOfClasses = new List<Class>();
            using (SqlConnection dataConnection = new SqlConnection(this.ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = "SELECT * FROM CLASSES";

                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    Class c = new Class();
                    c.classID = dataReader.GetInt32(0);
                    c.subjectID = dataReader.GetInt32(1);
                    c.date = dataReader.GetDateTime(2).Date;
                    c.isLecture = dataReader.GetBoolean(3);
                    c.lecturerID = dataReader.GetInt32(4);
                    c.isOpen = dataReader.GetBoolean(5);
                    c.classPass = dataReader.GetString(6);
                    c.practiceGroup = dataReader.GetString(7);
                    listOfClasses.Add(c);
                }
            }

            return listOfClasses;
        }

        // Insert casova
        public int insertClass(Class c)
        {
            using (SqlConnection dataConnection = new SqlConnection(ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;

                command.CommandText = "INSERT INTO CLASSES VALUES (" +
                      +c.subjectID + ", " + "'" + c.date + "'" + ", " + "'" + c.isLecture + "'" + ", " + c.lecturerID + ", " + "'" + c.isOpen + "'" + ", " + "'" + c.classPass + "'" + ", " + "'" + c.practiceGroup + "'" + ")";
                System.Diagnostics.Debug.WriteLine("SQL UPIT -----------------" + command.CommandText);
                return command.ExecuteNonQuery();
            }

        }

        // Update casova
        public int updateClass(Class c)
        {
            using (SqlConnection dataConnection = new SqlConnection(ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;

                command.CommandText = "UPDATE CLASSES SET" +
                    " subjectID = " + c.subjectID + "," +
                    " date = " + "'" + c.date + "'" + "," +
                    " lecture = " + "'" + c.isLecture + "'" +
                    "," + " lectureID = " + c.lecturerID + "," +
                    " isOpen = " + "'" + c.isOpen + "'" + "," + " classPassword = " + "'" + c.classPass + "'" + "," +
                    " practiceGroup = " + "'" + c.practiceGroup + "'" + " WHERE classID = " + c.classID;
                System.Diagnostics.Debug.WriteLine("SQL UPIT -----------------" + command.CommandText);

                return command.ExecuteNonQuery();
            }
        }

        public int deleteClass(int classID)
        {
            using (SqlConnection dataConnection = new SqlConnection(ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;

                command.CommandText = "DELETE FROM CLASSES WHERE classID = " + classID;
                return command.ExecuteNonQuery();

            }
        }
        public List<Class_Subject> getAllClassesForDay() // JOIN tabela classes i subjects za 
        {
            List<Class_Subject> listOfClassesForDay = new List<Class_Subject>();
            using (SqlConnection dataConnection = new SqlConnection(this.ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = "SELECT s.subjectName,  c.date,c.practiceGroup, c.isOpen, c.classPassword, c.classID FROM CLASSES c INNER JOIN SUBJECTS s ON c.subjectID = s.subjectID";

                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    Class_Subject cs = new Class_Subject();

                    cs.subjectName = dataReader.GetString(0);
                    cs.date = dataReader.GetDateTime(1);
                    cs.practiceGroup = dataReader.GetString(2);
                    cs.isOpen = dataReader.GetBoolean(3);
                    cs.classPassword = dataReader.GetString(4);
                    cs.classID = dataReader.GetInt32(5);
                    listOfClassesForDay.Add(cs);
                }
            }

            return listOfClassesForDay;
        }

    }
}

