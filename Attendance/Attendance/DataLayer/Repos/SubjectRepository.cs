﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Attendance.DataLayer.Models;


namespace Attendance.DataLayer.Repos
{
    public class SubjectRepository : ISubjectRepository
    {
        public string ConnectionString;

        public void GetConnectionString(string connString)
        {
            ConnectionString = connString;
        }

        public List<Subject> GetAllSubjects()
        {
            List<Subject> listOfSubjects = new List<Subject>();
            using (SqlConnection dataConnection = new SqlConnection(this.ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = "SELECT * FROM SUBJECTS";

                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    Subject s = new Subject();

                    s.subjectID = dataReader.GetInt32(0);
                    s.subjectName = dataReader.GetString(1);
                    s.professorID = dataReader.GetInt32(2);
                    s.assistantID = dataReader.GetInt32(3);
                    s.lectureNo = dataReader.GetInt32(4);
                    s.exerciseNo = dataReader.GetInt32(5);
                    s.subjectDescription = dataReader.GetString(6);

                    listOfSubjects.Add(s);
                }
            }

            return listOfSubjects;
        }
    }

    

}
